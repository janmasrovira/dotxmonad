module Main (main) where

import Base
import DotXmonad qualified as D

main :: IO ()
main = D.main
