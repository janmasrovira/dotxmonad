{-# LANGUAGE UndecidableInstances #-}

module Headphones where

import Base

data HeadphoneBrand
  = Focal
  | Hifiman
  | Sennheiser
  | ThieAudio
  | UniqueMelody
  deriving stock (Eq, Ord)

$(genSingletons [''HeadphoneBrand])

data HifimanModel
  = HE6seV2
  | Arya
  deriving stock (Eq, Ord)

data FocalModel
  = ClearMG
  deriving stock (Eq, Ord)

data ThieAudioModel
  = Monarch
  deriving stock (Eq, Ord)

data UniqueMelodyModel
  = MestMKII
  deriving stock (Eq, Ord)

data SennheiserModel
  = HD599
  deriving stock (Eq, Ord)

type HeadphoneModelType :: HeadphoneBrand -> GHCType
type family HeadphoneModelType s = res | res -> s where
  HeadphoneModelType 'Hifiman = HifimanModel
  HeadphoneModelType 'Focal = FocalModel
  HeadphoneModelType 'ThieAudio = ThieAudioModel
  HeadphoneModelType 'UniqueMelody = UniqueMelodyModel
  HeadphoneModelType 'Sennheiser = SennheiserModel

newtype HeadphoneModel (b :: HeadphoneBrand)
  = HeadphoneModel
      (HeadphoneModelType b)

instance (Show (HeadphoneModelType b)) => Show (HeadphoneModel b) where
  show (HeadphoneModel b) = show b

instance (Eq (HeadphoneModelType b)) => Eq (HeadphoneModel b) where
  (HeadphoneModel b) == (HeadphoneModel b') = b == b'

instance (Ord (HeadphoneModelType b)) => Ord (HeadphoneModel b) where
  compare (HeadphoneModel b) (HeadphoneModel b') = compare b b'

newtype Headphone = Headphone
  { _unHeadphone :: Sigma HeadphoneBrand (TyCon1 HeadphoneModel)
  }

makeLenses ''Headphone

mkHeadphone :: forall (b :: HeadphoneBrand). SHeadphoneBrand b -> HeadphoneModelType b -> Headphone
mkHeadphone b m = Headphone (b :&: HeadphoneModel m)

compareHeadphone :: Headphone -> Headphone -> Ordering
compareHeadphone
  (Headphone (b1 :&: m1))
  (Headphone (b2 :&: m2)) = case compare (fromSing b1) (fromSing b2) of
    LT -> LT
    GT -> GT
    EQ -> case (b1, b2) of
      (SHifiman, SHifiman) -> compare m1 m2
      (SHifiman, _) -> impossible
      (SFocal, SFocal) -> compare m1 m2
      (SFocal, _) -> impossible
      (SThieAudio, SThieAudio) -> compare m1 m2
      (SThieAudio, _) -> impossible
      (SUniqueMelody, SUniqueMelody) -> compare m1 m2
      (SUniqueMelody, _) -> impossible
      (SSennheiser, SSennheiser) -> compare m1 m2
      (SSennheiser, _) -> impossible

instance Eq Headphone where
  h1 == h2 = compareHeadphone h1 h2 == EQ

instance Ord Headphone where
  compare = compareHeadphone

headphoneBrand :: SimpleGetter Headphone HeadphoneBrand
headphoneBrand = to (fstSigma . (^. unHeadphone))

headphoneModelShow :: Headphone -> String
headphoneModelShow (Headphone (b :&: m)) = case b of
  SHifiman -> show m
  SFocal -> show m
  SThieAudio -> show m
  SUniqueMelody -> show m
  SSennheiser -> show m

instance Show SennheiserModel where
  show = \case
    HD599 -> "HD 599"

instance Show UniqueMelodyModel where
  show = \case
    MestMKII -> "Mest MKII"

instance Show ThieAudioModel where
  show = \case
    Monarch -> "Monarch"

instance Show FocalModel where
  show = \case
    ClearMG -> "Clear MG"

instance Show HifimanModel where
  show = \case
    Arya -> "Arya"
    HE6seV2 -> "HE6se V2"

instance Show Headphone where
  show h =
    show (h ^. headphoneBrand)
      <> " "
      <> headphoneModelShow h

instance Show HeadphoneBrand where
  show = \case
    Hifiman -> "Hifiman"
    Focal -> "Focal"
    ThieAudio -> "ThieAudio"
    UniqueMelody -> "UM"
    Sennheiser -> "Sennheiser"
