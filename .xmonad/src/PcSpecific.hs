module PcSpecific where

import Base
import Data.Map.Strict as Map
import XMonad

data PcSpecific = PcSpecific
  { _isNvidiaGpu :: Bool,
    _alias :: String,
    _machineId :: String,
    _edp :: String,
    _hdmi :: String
  }
  deriving stock (Show)

makeLenses ''PcSpecific

getCurrentPc :: X (Maybe PcSpecific)
getCurrentPc = (`Map.lookup` pcs) . rstrip <$> liftIO (readFile "/etc/machine-id")

pcs :: Map String PcSpecific
pcs = fromList [(p ^. machineId, p) | p <- everyPc]
  where
    everyPc =
      [ PcSpecific
          { _alias = "Asus",
            _isNvidiaGpu = True,
            _machineId = "92b63f78e8f040d190a25d2a69e043c6",
            _edp = "eDP1",
            _hdmi = "HDMI-1-0"
          },
        PcSpecific
          { _alias = "Delta 15",
            _isNvidiaGpu = False,
            _machineId = "e56ce01507684545b1811dd8571b02cf",
            _edp = "eDP",
            _hdmi = "HDMI-A-1-0"
          },
        PcSpecific
          { _alias = "Creator M16",
            _isNvidiaGpu = False, -- TODO revise
            _machineId = "3a934684e0524471b23d5d206dbd28d2",
            _edp = "eDP-2",
            _hdmi = "HDMI-1-0"
          }
      ]
