module GridSelect (gridSelectKeys) where

import Base
import Data.Function
import Data.Map qualified as M
import XMonad
import XMonad.Actions.GridSelect

gridSelectKeys :: [((KeyMask, KeySym), X ())]
gridSelectKeys = [((modm, xK_g), goToSelected def {gs_navigate = myNavigation})]

myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
  where
    navDefaultHandler :: b -> TwoD x (Maybe x)
    navDefaultHandler = const myNavigation

navKeyMap :: M.Map (KeyMask, KeySym) (TwoD a (Maybe a))
navKeyMap = M.union customNavKeyMap defaultNavKeyMap

customNavKeyMap :: M.Map (KeyMask, KeySym) (TwoD a (Maybe a))
customNavKeyMap =
  M.fromList
    [ ((controlMask, xK_g), cancel),
      ((controlMask, xK_f), mySubstringSearch myNavigation)
    ]

-- | Adapted from the source code.
defaultNavKeyMap :: M.Map (KeyMask, KeySym) (TwoD a (Maybe a))
defaultNavKeyMap =
  M.fromList
    [ ((0, xK_Escape), cancel),
      ((0, xK_Return), select),
      ((0, xK_slash), mySubstringSearch next),
      ((0, xK_Left), move (-1, 0) >> next),
      ((0, xK_h), move (-1, 0) >> next),
      ((0, xK_Right), move (1, 0) >> next),
      ((0, xK_l), move (1, 0) >> next),
      ((0, xK_Down), move (0, 1) >> next),
      ((0, xK_j), move (0, 1) >> next),
      ((0, xK_Up), move (0, -1) >> next),
      ((0, xK_k), move (0, -1) >> next),
      ((0, xK_Tab), moveNext >> next),
      ((0, xK_n), moveNext >> next),
      ((shiftMask, xK_Tab), movePrev >> next),
      ((0, xK_p), movePrev >> next)
    ]
  where
    next :: TwoD a (Maybe a)
    next = myNavigation

mySubstringSearch :: TwoD a (Maybe a) -> TwoD a (Maybe a)
mySubstringSearch returnNavigation = fix $ \me ->
  let searchKeyMap =
        M.fromList
          [ ((controlMask, xK_g), transformSearchString (const "") >> returnNavigation),
            ((0, xK_Escape), transformSearchString (const "") >> returnNavigation),
            ((0, xK_Return), returnNavigation),
            ((0, xK_BackSpace), transformSearchString (\s -> if null s then "" else init s) >> me)
          ]
      searchDefaultHandler (_, s, _) = do
        transformSearchString (++ s)
        me
   in makeXEventhandler $ shadowWithKeymap searchKeyMap searchDefaultHandler
