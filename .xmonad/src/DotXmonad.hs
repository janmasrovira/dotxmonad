module DotXmonad where

import Audio
import Base
import EasyEffects
import GridSelect
import PcSpecific
import SteamLauncher (gameMenu)
import System.Exit
import XMonad
import XMonad.Actions.NoBorders
import XMonad.Actions.PhysicalScreens
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Layout.Grid
import XMonad.Layout.LayoutModifier
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.ToggleLayouts as L
import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce

myBaseConfig ::
  XConfig
    ( ModifiedLayout
        AvoidStruts
        (Choose Tall (Choose (Mirror Tall) Full))
    )
myBaseConfig = desktopConfig

myConfig ::
  XConfig
    ( ModifiedLayout
        AvoidStruts
        ( ModifiedLayout
            (ConfigurableBorder Ambiguity)
            ( ModifiedLayout
                Spacing
                ( ToggleLayouts
                    Full
                    ( Choose
                        ( ModifiedLayout
                            AvoidStruts
                            (Choose Tall (Choose (Mirror Tall) Full))
                        )
                        Grid
                    )
                )
            )
        )
    )
myConfig =
  docks
    . ewmh
    $ myBaseConfig
      { manageHook = manageHook myBaseConfig,
        layoutHook =
          avoidStruts
            . lessBorders Screen
            $ ModifiedLayout
              mySpacing
              (toggleLayouts Full (layoutHook myBaseConfig ||| Grid)),
        modMask = modm,
        terminal = "alacritty",
        logHook = dynamicLogString def >>= xmonadPropLog,
        handleEventHook = handleEventHook myBaseConfig,
        focusedBorderColor = "#499ffc",
        borderWidth = 1,
        startupHook = startupHook myBaseConfig >> myStartupHook
      }
      `additionalKeys` additionalKeysList
  where
    mySpacing :: Spacing Window
    mySpacing =
      Spacing
        { smartBorder = True,
          screenBorder = uniformBorder 0,
          screenBorderEnabled = False,
          windowBorder = uniformBorder 2,
          windowBorderEnabled = True
        }
    uniformBorder :: Integer -> Border
    uniformBorder i = Border i i i i

additionalKeysList :: [((KeyMask, KeySym), X ())]
additionalKeysList =
  concat
    [ volumeKeys,
      appKeys,
      layoutKeys,
      gridSelectKeys,
      navKeys,
      otherKeys
    ]

-- | Navigation of physical screens by its relative position. See
-- 'XMonad.Actions.PhysicalScreens'.
navKeys :: [((KeyMask, KeySym), X ())]
navKeys =
  [ ((modm, xK_w), viewScreen def (P 0)),
    ((modm, xK_e), viewScreen def (P 1)),
    ((shiftMask .|. modm, xK_w), sendToScreen def (P 0)),
    ((shiftMask .|. modm, xK_e), sendToScreen def (P 1))
  ]

-- | Layouts
layoutKeys :: [((KeyMask, KeySym), X ())]
layoutKeys =
  []

-- | Other keys
otherKeys :: [((KeyMask, KeySym), X ())]
otherKeys =
  [ ((shiftMask .|. modm, xK_q), askToQuit),
    ((shiftMask .|. modm, xK_i), askInputMethod)
  ]

-- | Applications
appKeys :: [((KeyMask, KeySym), X ())]
appKeys =
  -- firefoxKeys ++
  braveKeys
    ++ [ ((modm, xK_v), spawn "tdrop -a alacritty"),
         ((shiftMask .|. modm, xK_v), spawn "alacritty --command fish --init-command tmux new"),
         ((0, musicKey), spawnMusicPlayer),
         ((shiftMask .|. modm, xK_m), spawnMusicPlayer),
         ((shiftMask .|. modm, xK_o), askSetDefaultSink),
         ((shiftMask .|. modm, xK_n), say "file browser" >> spawn "thunar"),
         ((shiftMask .|. modm, xK_d), say "slack" >> spawn "slack"),
         ((shiftMask .|. modm, xK_x), spawn "emacsclient -c"),
         ((shiftMask .|. modm, xK_l), askFlameshot),
         ((shiftMask .|. modm, xK_h), easyEffectMenu),
         ((shiftMask .|. modm, xK_j), say "lol highlight" >> spawn "fish --command=\"nospoiler --clipboard\""),
         ((shiftMask .|. modm, xK_p), spawn "rofi -show drun -show-icons"),
         ((modm, xK_p), gameMenu),
         ((modm, xK_o), spawn "rofi -show window -show-icons"),
         ((modm, xK_z), sendMessage (SetStruts [] [minBound .. maxBound]) >> spawn "polybar-msg cmd hide"),
         ((modm, xK_b), sendMessage (SetStruts [minBound .. maxBound] []) >> spawn "polybar-msg cmd show"),
         ((shiftMask .|. modm, xK_f), sendMessage (L.Toggle "Full")),
         ((shiftMask .|. modm, xK_t), withFocused toggleBorder)
       ]

firefoxKeys :: [((KeyMask, KeySym), X ())]
firefoxKeys =
  [ ((shiftMask .|. modm, xK_a), spawn "firefox --new-window blank.org"),
    ((shiftMask .|. modm, xK_s), spawn "firefox --private-window")
  ]

braveKeys :: [((KeyMask, KeySym), X ())]
braveKeys =
  [ ((shiftMask .|. modm, xK_a), spawn "brave"),
    ((shiftMask .|. modm, xK_s), spawn "brave --incognito")
  ]

askFlameshot :: X ()
askFlameshot =
  askListDoSimple
    [ ("GUI", spawn "flameshot gui"),
      ("Current Screen", spawn "flameshot screen -r -p ~/documents"),
      ("Full", spawn "flameshot full -p ~/documents")
    ]

askToQuit :: X ()
askToQuit =
  askListDoSimple
    [ ("Logout", io exitSuccess),
      ("Reboot", spawn "reboot"),
      ("Shutdown", spawn "poweroff"),
      ("Suspend", spawn "systemctl suspend")
    ]

askInputMethod :: X ()
askInputMethod =
  askListDoSimple
    [ ("Japanese", spawn "fcitx5-remote -s mozc"),
      ("Catalan", spawn "fcitx5-remote -s keyboard-es-cat")
    ]

myStartupHook :: X ()
myStartupHook = do
  pc <- getCurrentPc
  whenJust pc monitorSetup
  whenJust pc welcome
  spawnOnce "copyq &"
  spawnOnce "easyeffects com.github.wwmm.easyeffects --gapplication-service &"
  spawnOnce "fish --command='emacs --daemon' &"
  spawnPolybar
  spawnOnce "nm-applet &"
  spawnOnce "xfce4-power-manager &"
  spawnOnce "xset m 3/2 1"
  spawnOnce "feh --bg-scale ~/dotfiles/xmonad/.xmonad/black-solid-color-background.avif"
  where
    spawnPolybar :: X ()
    spawnPolybar = spawnOnce "polybar main &"

    welcome :: (MonadIO m) => PcSpecific -> m ()
    welcome PcSpecific {..} = say $ "welcome to " <> _alias

    monitorSetup :: PcSpecific -> X ()
    monitorSetup PcSpecific {..} = do
      spawnOnce $
        intercalate
          " && "
          [ "xrandr --output " <> _edp <> " --primary",
            "xrandr --output " <> _hdmi <> " --auto",
            "xrandr --output " <> _hdmi <> " --left-of " <> _edp
          ]

main :: IO ()
main = do
  greet
  xmonad myConfig
  where
    greet :: IO ()
    greet = say "HejHej"
